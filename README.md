# Bootstrap 4

Front-End Web UI Frameworks and Tools: Bootstrap 4
Université des sciences et technologies de Hong Kong
30 JAN 2021 

IDE : Visual Studio Code 
Extension : Bootstrap 4 snippets based on documentation + Font awesome 4 + Font Awesome 5 Free & Pro snippets v6.1.0

Ressources : 
https://css-tricks.com/why-npm-scripts/
https://github.com/damonbauer/npm-build-boilerplate
https://www.keithcirkel.co.uk/how-to-use-npm-as-a-build-tool/
https://webdesign.tutsplus.com/series/the-command-line-for-web-design--cms-777
https://www.anthedesign.fr/developpement-web/tasks-runner-developpeur-front/


NPM Modules
onchange
parallelshell
rimraf
copyfiles
imagemin-cli
usemin-cli
cssmin
uglifyjs
htmlmin
