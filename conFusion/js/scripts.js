$(document).ready(function () {
    $('#carouselId').carousel({interval : 1000});

    $('#carouselButton').click(function () {

        if($('#carouselButton').children('span').hasClass('fa-pause')) {
            $('#carouselId').carousel('pause');
            $('#carouselButton').children('span').removeClass('fa-pause');
            $('#carouselButton').children('span').addClass('fa-play');
        } else if($('#carouselButton').children('span').hasClass('fa-play')) {
            $('#carouselId').carousel('cycle');
            $('#carouselButton').children('span').removeClass('fa-play');
            $('#carouselButton').children('span').addClass('fa-pause');
        }

    });
    
    // Login modal. 
    $('#loginButton').on('click', function name(params) {
        $('#loginModal').modal('toggle');
    });

    // Reserve table modal. 
    $('#reserveButton').on('click', function name(params) {
        $('#ReserveTableModal').modal('toggle');
    });
});
